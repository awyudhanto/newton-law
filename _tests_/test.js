const { execSync } = require('child_process')
const fs = require('fs')

const reconstructedFilename = 'reconstructed.js'

const resultan_gaya = (massa, percepatan) => {
  let solution = fs.readFileSync('./index.js', 'utf-8')

  solution = solution.replace(/(let|var) massa .*/, `$1 massa = ${massa}`)
  solution = solution.replace(/(let|var) percepatan .*/, `$1 percepatan = ${percepatan}`)

  fs.writeFileSync(reconstructedFilename, solution)

  return String(execSync(`node ${reconstructedFilename}`))
}

afterAll(() => {
  if (fs.existsSync(reconstructedFilename)) {
    fs.unlinkSync(reconstructedFilename)
  }
})

describe('Newton Second Law', () => {
  describe('Check resultan gaya', () => {
    it('should print 200 when massa = 10 and percepatan = 20', () => {
      const result = resultan_gaya(10, 20)
      expect(result).toMatch(/200/i)
    })
    it('should print -200 when massa = 10 and percepatan = -20', () => {
      const result = resultan_gaya(10, -20)
      expect(result).toMatch(/-200/i)
    })
    it('should print 17500 when massa = 17500 and percepatan = 1', () => {
      const result = resultan_gaya(17500, 1)
      expect(result).toMatch(/17500/i)
    })
    it('should print 52500 when massa = 17500 and percepatan = 3', () => {
      const result = resultan_gaya(17500, 3)
      expect(result).toMatch(/52500/i)
    })
    it('should print 0 when massa = 0 and percepatan = 1', () => {
      const result = resultan_gaya(0, 1)
      expect(result).toMatch(/0/i)
    })
  })
})
